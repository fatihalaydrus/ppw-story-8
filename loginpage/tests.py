from importlib import import_module

from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from django.http import request
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.conf import settings
from django.apps import apps

from .views import login, logout
from .apps import LoginpageConfig

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

class UnitTest(TestCase):
    def test_loginpage_app_is_exist(self):
        self.assertEqual(LoginpageConfig.name, 'loginpage')
        self.assertEqual(apps.get_app_config('loginpage').name, 'loginpage')
    
    def test_loginpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)


    def test_loginpage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, login)
        found = resolve('/login/')
        self.assertEqual(found.func, login)
        found = resolve('/logout/')
        self.assertEqual(found.func, logout)
    
    def test_loginpage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

        #test opening the web
    def test_open(self):
        self.browser.get(self.live_server_url)
        self.assertEqual(self.browser.title, "Login Page")
        self.assertIn("Login", self.browser.page_source)

    def test_login(self):
        self.browser.get(self.live_server_url)
        username = self.browser.find_element_by_id("id_username")
        username.send_keys("fatih")
        password = self.browser.find_element_by_id("id_password")
        password.send_keys("1")
        password.submit()
        self.assertIn("fatih", self.browser.page_source)



 