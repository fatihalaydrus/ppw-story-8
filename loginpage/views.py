from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as login_auth, logout as logout_auth

# Create your views here.
def login(request):
    user = request.user
    login_form = AuthenticationForm()
    
    if user.is_authenticated:
        return render(request, 'dashboard.html')

    if request.method == "POST":
        login_form = AuthenticationForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login_auth(request, user)
            return redirect('login')

    return render(request, 'login.html', {'form':login_form})

def logout(request):
    if request.method == 'POST':
        logout_auth(request)
    return redirect('login')
