$(function(){
    // jQuery methods go here...
	$("#myInput").on("keypress", function(e) {
		query = e.currentTarget.value.toLowerCase()
        $.ajax({
            url: "data/?q=" + query,
            datatype: 'json',
            success: function(data){
            $('tbody').html('');
            var bookResult;
            for(var i = 0; i < data.items.length; i++) {
                bookResult += "<tr><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img></td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td></tr>";
            }
            $('tbody').append(bookResult);
            }
        });
    });
});