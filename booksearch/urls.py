from django.contrib import admin
from django.urls import path
from .views import index, data

urlpatterns = [
    path('', index, name="booksearch"),
    path('data/', data, name="data_call"),
]