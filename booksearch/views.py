from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

# Create your views here.

def index(request):
    return render(request, 'index.html')

def data(request):
	try: q = request.GET['q']
	except: q = 'quilting'
	jsonInput = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()
	return JsonResponse(jsonInput)


